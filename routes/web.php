<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return "home";
});

Route::get('/about', function() {
    return "About";
});

Route::get('/contact', function () {
    return "Contact";
});

// barengsaya.com/post/post-satu
// barengsaya.com/post/detail-post

Route::prefix('post')->group(function() {
    Route::get('/judul-post', function() {
        return "ini judul postingan";
    });
    Route::get('/judul-post/detail', function () {
        return "ini detail postingan";
    });
});

Route::get('/name', function() {
    return "pengenalan route name";
})->name('route-name');

// barengsaya.com/post/3
Route::get('users/{id}', function ($id) {
    return "user dengan id : " . $id;
});